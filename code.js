const API_URL = "https://cat-fact.herokuapp.com";

fetch(`${API_URL}/facts/random?amount=3`)
    .then(response => response.json())
    .then(data => showCatFacts(data));

function showCatFacts(facts) {
    var catFactList = document.getElementById("cat-facts");
    for (let fact of facts) {
        let catFact = document.createElement("li");
        catFact.innerText = fact.text;
        catFactList.appendChild(catFact);
    }
}